#!/bin/bash
COUNTER=0
{
	read # skip first line
	while read p; do
		lat=$(echo "$p" | cut -d, -f1)
		long=$(echo "$p" | cut -d, -f2)
		data=$(curl -s "https://www.spar.at/standorte/_jcr_content.stores.v2.html?latitude=$lat&longitude=$long&radius=0&hour=&filter=" | jq -r '.[] | .pageUrl')
		echo "https://www.spar.at$data" >> spar_links.txt
		#echo "$data" >> spar_addresses.txt
		let COUNTER++
		echo "$COUNTER"
	done
} <../spar_locations.csv

