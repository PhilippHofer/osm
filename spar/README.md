> Goal: Try to make maintainance of spar locations + opening hours as robust and effortless as possible.

## High-Level Todo List

- [ ] Create all stores (see Todo list below)
- [ ] Enter opening hours for all stores
- [ ] Keep track of changes (locations + opening hours, probably with local version of changedetection.io)

## Data

There are ~1519 spars in Austria[1]. You can crawl a list with lat/long entries of every spar with `curl -s "https://www.spar.at/standorte/_jcr_content.marker.v2.html?latitude=48.331142&longitude=14.312629&radius=400000000000&hour=&filter="  | jq -r '.[] | "\(.latitude),\(.longitude)"'`, which yields 1557 entries (as of 2022-08-22), including one entry with 0,0 lat/long. Cleaned list is available in `spar_locations.csv`.

You can visualize all spar markets on a rough Austria map in `vis_spar_locations/`.

You can fetch the addresses of the spar markets with `details_spar/`

### Summary
- 1556 spar locations (`spar_locations.csv`)
- 1611 addresses (in `details_spar/spar_addresses.txt`), because there can be multiple spar's at the same location (e.g. ITNERSPAR + restaurant at Agathastraße 1)

## Todo List [/572]
Maßwegerstraße 54, 8724 Spielberg (SPAR Markt)
Nr. 66, 8505 St. Nikolai im Sausal (SPAR Supermarkt)
Engerthstraße 214F/1A, 1020 Wien (EUROSPAR)
Erdbergstraße 52-60, 1030 Wien (EUROSPAR)
Rennweg 46-50, 1030 Wien (EUROSPAR)
Kreuzgasse 72-74, 1180 Wien (EUROSPAR)
Donaufelderstraße 150, 1220 Wien (EUROSPAR)
Gatterederstraße 17, 1230 Wien (EUROSPAR)
Aubauer Straße 35, 4810 Gmunden (EUROSPAR)
Innsbruckerstraße 83, 6060 Hall in Tirol (EUROSPAR)
Löwenpark 1, Top 15, 3390 Melk (EUROSPAR)
Hauserstraße 15a, 4724 Neukirchen am Walde (EUROSPAR)
Almtalstr. 1, 4643 Pettenbach (EUROSPAR)
Ötscherlandstraße 14, 3251 Purgstall (EUROSPAR)
Mühlerstraße 20, 6600 Reutte (EUROSPAR)
Südtirolerplatz 13, 5020 Salzburg (EUROSPAR)
Friedensstraße 4, 5020 Salzburg (EUROSPAR)
Sterneckstraße 35-37, 5020 Salzburg (EUROSPAR)
Salzburger Straße 27, 4690 Schwanenstadt (EUROSPAR)
Hauptstraße 4, 5201 Seekirchen (EUROSPAR)
Atterseestr. 7, 4863 Seewalchen (EUROSPAR)
Hans-Kappacher-Straße 1, 5600 St. Johann i.Pg. (EUROSPAR)
Werkstraße 34, 4300 St. Valentin (EUROSPAR)
A. Thalhammerstraße 22, 5204 Straßwalchen (EUROSPAR)
Umfahrungsstraße 9, 9100 Völkermarkt (EUROSPAR)
Hauptstraße 70, 8740 Zeltweg (EUROSPAR)
Mariahilferstraße 77-79, 1060 Wien (EUROSPAR)
Wilhelm Redl Straße 2, 4770 Andorf (EUROSPAR)
Diesseits 244, 4973 St. Martin (EUROSPAR)
Goetzstraße 8-10, 4820 Bad Ischl (EUROSPAR)
ECO Plus Park 4te. Straße, 2460 Bruck/Leitha (EUROSPAR)
Wiener Straße 2-4, 5301 Eugendorf (EUROSPAR)
Smolekstraße 2, 2401 Fischamend (EUROSPAR)
Wiener Straße 3, 3250 Wieselburg (EUROSPAR)
Volksgartenstraße 3-5, 8020 Graz (EUROSPAR)
Innovationsplatz 2, 7051 Großhöflein (EUROSPAR)
Bundesstraße 4b, 9241 Wernberg (EUROSPAR)
Hauptplatz 19, 8700 Leoben (EUROSPAR)
Humerstraße 16, 4063 Hörsching (EUROSPAR)
Bruck an der Aschach 34, 4722 Peuerbach (EUROSPAR)
Feldgasse 4-12, 2460 Bruck an der Leitha (EUROSPAR)
Bahnhofstraße 9, 3350 Stadt Haag (EUROSPAR)
Alpenstraße 114, 5020 Salzburg (EUROSPAR)
Krinningergasse 10, 2620 Neunkirchen (EUROSPAR)
IntroPark EKZ 1 / 12, 7011 Siegendorf (EUROSPAR)
Reichstraße 91, 8472 Vogau (EUROSPAR)
Bahnhofstraße 11, 3370 Ybbs an der Donau (EUROSPAR)
Süduferstraße 241, 9523 Villach (SPAR Supermarkt)
Ainet 86a, 9951 Ainet (SPAR Supermarkt)
Absberggasse 26, 1100 Wien (SPAR Express)
Wölzing F 13, 9433 St.Andrä/Lavanttal (SPAR Markt)
Stadionstraße 4, 9545 Radenthein (EUROSPAR)
Tiroler Straße 10, 9500 Villach (SPAR Express)
Dorfplatz 11, 9423 St. Georgen / Lavanttal (SPAR Markt)
Winklern 215, 9841 Winklern (SPAR Supermarkt)
Sillian 11, 9920 Sillian (SPAR Markt)
Leonhardsplatz 4, 6800 Feldkirch (INTERSPAR)
Leonhardsplatz 4, 6800 Feldkirch (Restaurant)
Könighofstraße 79, 6800 Feldkirch (INTERSPAR)
Wienerstraße 286, 8051 Graz (INTERSPAR)
Wienerstraße 286, 8051 Graz (Restaurant)
Langgasse 38, 6460 Imst (INTERSPAR)
Amraser Seestraße 56a, 6020 Innsbruck (INTERSPAR)
Rosentaler Straße 136, 9020 Klagenfurt (INTERSPAR)
Wienerstraße 91, 3500 Krems (INTERSPAR)
Großglocknerstraße 1, 9990 Lienz-Nussdorf (Restaurant)
Hüttendorf 189, 2130 Mistelbach (INTERSPAR)
Europastraße 2, 7400 Oberwart (INTERSPAR)
Europastraße 2, 7400 Oberwart (Restaurant)
Pluskaufstraße 7, 4061 Pasching (INTERSPAR)
Schumacherstraße 15, 5020 Salzburg-Lehen (INTERSPAR)
Schumacherstraße 15, 5020 Salzburg-Lehen (Restaurant)
Herm.Berghoferstraße 12, 6130 Schwaz (INTERSPAR)
Herm.Berghoferstraße 12, 6130 Schwaz (Restaurant)
Kärntner Straße 34, 9500 Villach (INTERSPAR)
Kärntner Straße 34, 9500 Villach (Restaurant)
Linzerstraße 50, 4840 Vöcklabruck (INTERSPAR)
SCS, 2334 Vösendorf (INTERSPAR)
Anton Baumgartnerstraße 40, 1230 Wien - Alterlaa (INTERSPAR)
Anton Baumgartnerstraße 40, 1230 Wien - Alterlaa (Restaurant)
Am Hauptbahnhof 1, UG1.121, 1100 Wien (INTERSPAR)
Landstrasser Hauptstraße 1b, 1030 Wien (INTERSPAR)
Donaustadtstraße 1, 1220 Wien (INTERSPAR)
Donaustadtstraße 1, 1220 Wien (Restaurant)
Gudrunstraße 102, Stiege 1, 1100 Wien (INTERSPAR)
Wagramer Straße 195, 1210 Wien (INTERSPAR)
Leonhardsplatz 4, 6800 Feldkirch (INTERSPAR)
Leonhardsplatz 4, 6800 Feldkirch (Restaurant)
Könighofstraße 79, 6800 Feldkirch (INTERSPAR)
Wienerstraße 286, 8051 Graz (INTERSPAR)
Wienerstraße 286, 8051 Graz (Restaurant)
Gudrunstraße 102, Stiege 1, 1100 Wien (Restaurant)
Rosentaler Straße 136, 9020 Klagenfurt (Restaurant)
Großglocknerstraße 1, 9990 Lienz-Nussdorf (Restaurant)
Hüttendorf 189, 2130 Mistelbach (Restaurant)
Europastraße 2, 7400 Oberwart (INTERSPAR)
Europastraße 2, 7400 Oberwart (Restaurant)
Schumacherstraße 15, 5020 Salzburg-Lehen (INTERSPAR)
Schumacherstraße 15, 5020 Salzburg-Lehen (Restaurant)
Herm.Berghoferstraße 12, 6130 Schwaz (INTERSPAR)
Herm.Berghoferstraße 12, 6130 Schwaz (Restaurant)
Kärntner Straße 34, 9500 Villach (INTERSPAR)
Kärntner Straße 34, 9500 Villach (Restaurant)
Linzerstraße 50, 4840 Vöcklabruck (Restaurant)
Anton Baumgartnerstraße 40, 1230 Wien - Alterlaa (INTERSPAR)
Anton Baumgartnerstraße 40, 1230 Wien - Alterlaa (Restaurant)
Donaustadtstraße 1, 1220 Wien (INTERSPAR)
Donaustadtstraße 1, 1220 Wien (Restaurant)
Langgsasse 38, 6460 Imst (Restaurant)
Landstrasser Hauptstraße 1b, 1030 Wien (INTERSPAR)
Am Hauptbahnhof 1, EG134, 1100 Wien (INTERSPAR)
Haslau 10, 4715 Taufkirchen/Trattnach (SPAR Express)
Prangerstrasse 2, 7091 Breitenbrunn (SPAR Markt)
Wienerstraße 1, 9360 Friesach (SPAR Markt)
Hauptstraße 34, 9322 Micheldorf (SPAR Markt)
Dorf 51, 6943 Riefensberg (SPAR Markt)
Sirnitz 5, 9571 Sirnitz (SPAR Markt)
Hausbach 51, 3900 Schwarzenau (SPAR Markt)
Lichtenau 63, 3522 Lichtenau (SPAR Markt)
Wolfshoferamt 148, 3572 Wolfshoferamt (SPAR Markt)
Bahnhofstraße 110, 3921 Langschlag (SPAR Markt)
Hauptraße 81, 3683 Yspertal (SPAR Markt)
Schulstraße 20/1, 3623 Kottes (SPAR Markt)
Bahnstraße 14, 3664 Martinsberg (SPAR Markt)
Waldhausen 79, 3914 Waldhausen (SPAR Markt)
Zwettlerstraße 16, 3542 Gföhl (SPAR Supermarkt)
Badgasse 19, 3650 Pöggstall (SPAR Supermarkt)
Pater-Werner-Deibl-Straße 7, 3910 Zwettl (EUROSPAR)
Innervillgraten 79, 9932 Innervillgraten (SPAR Markt)
Obertilliach 37, 9942 Obertilliach (SPAR Markt)
Brennerstraße 67, 6156 Gries am Brenner (SPAR Markt)
Petersplatz 5, 8093 St. Peter am Ottersbach (SPAR Supermarkt)
Ludmannsdorf 6, 9072 Ludmannsdorf (SPAR Markt)
Landstrasser Hauptstraße 1b, 1030 Wien (Restaurant)
Gewerbestraße 2, 9141 Eberndorf (SPAR Supermarkt)
Tanglstraße 2, 9400 Wolfsberg (SPAR Supermarkt)
Klagenfurter Straße 23, 9170 Ferlach (SPAR Express)
Zwettler-Straße 133, 3970 Weitra (SPAR Supermarkt)
Thermenweg 2, 9530 Bad Bleiberg (SPAR Supermarkt)
Ernst-Melchior-Gasse 18, Top 1, 1020 Wien (SPAR Supermarkt)
Laxenburger Straße 140, 1100 Wien (SPAR Supermarkt)
Troststraße 11/4/EG1, 1100 Wien (SPAR Supermarkt)
Hetzendorfer Straße 103, 1120 Wien (SPAR Supermarkt)
Hietzinger Hauptstraße 32, 1130 Wien (SPAR Supermarkt)
Ottakringerstraße 54, 1170 Wien (SPAR Supermarkt)
Jägerstraße 58c-e, 1200 Wien (SPAR Supermarkt)
Donaufelderstraße 2-4, 1210 Wien (SPAR Supermarkt)
Haspingerplatz 1-2, 1210 Wien (SPAR Supermarkt)
Jedleseerstraße 96, 1210 Wien (SPAR Supermarkt)
Anton Sattlergasse 115A, 1220 Wien (SPAR Supermarkt)
Baslergasse 68-84, 1230 Wien (SPAR Supermarkt)
Kaltenleutgebner Straße 22-24, 1230 Wien (SPAR Supermarkt)
Marktplatz 21, 8625 Turnau (SPAR Markt)
Hofsteigstraße50, 6858 Schwarzach (SPAR Markt)
Haus Nr. 15, 6882 Schnepfau (SPAR Markt)
Grazerstraße 45, 8062 Kumberg (SPAR Markt)
Niederlehen 7, 6391 Fieberbrunn (SPAR Markt)
Halbturner Straße 32, 7163 Andau (SPAR Supermarkt)
Brückenplatz 1, 3741 Pulkau (SPAR Supermarkt)
Grautschenhof 35, 8684 Spital a.S. (SPAR Supermarkt)
Pettauerstraße 8, 8184 Anger b. Weiz (SPAR Supermarkt)
Haager Straße 60, 4400 Steyr (SPAR Supermarkt)
Nr. 150, 4906 Eberschwang (SPAR Markt)
Sulb 114, 8543 St. Martin i.S. (SPAR Markt)
Linzerstraße 4, 4274 Schönau / Mkr (SPAR Supermarkt)
Schaunbergstraße 9, 4081 Hartkirchen (SPAR Markt)
Nappersdorf 103, 2023 Nappersdorf (SPAR Markt)
Haus Nr. 59, 6752 Dalaas/Klostertal (SPAR Markt)
Atterseestraße 48, 4850 Timelkam (SPAR Supermarkt)
Mondsee-Bundesstraße 1, 5340 St. Gilgen (SPAR Supermarkt)
Marktplatz 6, 4293 Gutau (SPAR Markt)
Josef-Kaut-Straße 8-10, 5026 Salzburg (SPAR Markt)
Nr. 12, 8953 Donnersbach (SPAR Markt)
Markt 109, 5440 Golling (SPAR Markt)
Am Bach 20, 4170 Haslach (SPAR Supermarkt)
Triesterstrasse 14, 8755 Rothenthurm (SPAR Express)
Burggasse 42, 8750 Judenburg (SPAR Express)
Conrad v. Hötzendorfstraße 135, 8010 Graz (SPAR Express)
Kirchdorf 137, 6884 Damüls (SPAR Markt)
Nr. 16, 3610 Weinzierl (SPAR Markt)
Nr. 30, 4721 Altschwendt (SPAR Markt)
Anton-Hermann-Straße 3, 8073 Feldkirchen bei Graz (SPAR Express)
Gewerbegebiet 1, 6611 Heiterwang (SPAR Express)
Steinamangerstrasse 8, 7423 Pinkafeld (SPAR Express)
Aibl Nr. 72, 8552 Eibiswald (SPAR Markt)
Mariazeller Straße 13a, 3202 Hofstetten (SPAR Markt)
Nr. 145, 8524 Bad Gams (SPAR Markt)
Neutorstraße 21, 5020 Salzburg (SPAR Supermarkt)
Nr. 89, 6653 Bach (SPAR Markt)
Oberwang Nr. 187, 4882 Oberwang (SPAR Supermarkt)
Stolzgasse 4 - 6, 3204 Kirchberg/Pielach (SPAR Supermarkt)
Gamsleiten 1, 5562 Obertauern (SPAR Markt)
Nr. 17, 8685 Steinhaus (SPAR Markt)
Nr. 38, 4074 Stroheim (SPAR Markt)
Ignaz-Rieder-Kai 21, 5020 Salzburg (SPAR Markt)
Weyregger Straße 14, 4852 Weyregg (SPAR Markt)
Nr. 70, 2062 Seefeld (SPAR Markt)
Nr. 44, 8342 Gnas (SPAR Supermarkt)
Marktplatz 9, 4180 Zwettl/Rodl (SPAR Markt)
Feldstraße 26, 4611 Buchkirchen (SPAR Supermarkt)
Nr. 23, 4090 Engelhartszell (SPAR Markt)
Faberstraße 6, 5020 Salzburg (SPAR Markt)
Marktplatz 11, 3122 Gansbach (SPAR Markt)
Nr. 79, 2881 Trattenbach (SPAR Markt)
Wiener Straße 15, 2485 Wampersdorf (SPAR Markt)
Nr. 67, 8182 Puch b. Weiz (SPAR Markt)
Nr. 68c, 6234 Brandenberg (SPAR Markt)
Landstraßer Hauptstraße 97-101, 1030 Wien (SPAR Gourmet)
Wiedner Hauptstraße 73, 1040 Wien (SPAR Gourmet)
Josefstädter Straße 13, 1080 Wien (SPAR Gourmet)
Guglgasse 6/A-Top 6-12, 1110 Wien (SPAR Gourmet)
Lainzer Straße 142, 1130 Wien (SPAR Gourmet)
Hernalser Hauptstraße 214, 1170 Wien (SPAR Gourmet)
Gersthofer Straße 121-123, 1180 Wien (SPAR Gourmet)
Gymnasium Straße 30, 1180 Wien (SPAR Gourmet)
Terminal 3, Top 13, Ebene 0, Objekt 115, 1300 Wien-Flughafen (SPAR Gourmet)
Hauptstraße 13, 7000 Eisenstadt (SPAR Gourmet)
Obere Landstraße 15, 3500 Krems (SPAR Gourmet)
Hauptstraße 45, 2340 Mödling (SPAR Gourmet)
Marktplatz 19, 2380 Perchtoldsdorf (SPAR Gourmet)
Kremser Gasse 21, 3100 St. Pölten (SPAR Gourmet)
Landstraße 33, 3910 Zwettl (SPAR Gourmet)
Schwarzenbergplatz 16, 1010 Wien (SPAR Gourmet)
Burggasse 2, 1070 Wien (SPAR Gourmet)
Hietzinger Hauptstraße 145-147, 1130 Wien (SPAR Gourmet)
Währinger Straße 90-92, 1180 Wien (SPAR Gourmet)
Wiener Straße 2, 3300 Amstetten (SPAR Gourmet)
Stadtplatz 26, 3400 Klosterneuburg (SPAR Gourmet)
Betriebsgebiet Nord 1, 3701 Großweikersdorf (SPAR Supermarkt)
Hauptstraße 44, 5531 Eben im Pongau (SPAR Markt)
Unterer Markt 49, 4292 Kefermarkt (SPAR Supermarkt)
Pregartner Str. 1a/1, 4284 Tragwein (SPAR Supermarkt)
Hauptplatz 6, 8223 Stubenberg (SPAR Markt)
Hauptstraße 63, 3170 Hainfeld (SPAR Supermarkt)
Marktplatz 5, 4310 Mauthausen (SPAR Markt)
Dorf 58, 6154 Schmirn (SPAR Markt)
Nr. 48, 5431 Kuchl (SPAR Supermarkt)
Fischanger 78, 6932 Langen bei Bregenz (SPAR Markt)
Watschingergasse 1, 2630 Ternitz (SPAR Supermarkt)
Bahnhofsstraße 11, 2000 Stockerau (SPAR Supermarkt)
Haslacher Straße 2, 4171 St.Peter am Wimberg (SPAR Supermarkt)
Tauplitz 8, 8982 Tauplitz (SPAR Markt)
Dorfstraße 374, 5754 Hinterglemm (SPAR Markt)
Mühledörfle 89, 6708 Brand (SPAR Markt)
St. Pöltner Straße 40, 3130 Herzogenburg (SPAR Supermarkt)
Nr. 253, 8233 Lafnitz (SPAR Supermarkt)
Raabser Straße 64, 3580 Horn (SPAR Supermarkt)
Wienerstraße 1, 3580 Horn (SPAR Supermarkt)
HNR. 19a, 6182 Gries im Sellrain (SPAR Markt)
Nr. 15, 4573 Hinterstoder (SPAR Markt)
Markt 39B, 5602 Wagrain (SPAR Markt)
Hauptstrasse 136, 2213 Bockfliess (SPAR Supermarkt)
Bad Waltersdorf 246, 8271 Bad Waltersdorf (SPAR Supermarkt)
Nr. 161, 8341 Paldau (SPAR Markt)
Schlägler Hauptstraße 13, 4160 Aigen-Schlägl (EUROSPAR)
Pramet 58, 4925 Pramet (SPAR Supermarkt)
Arlbergstraße 55, 6751 Braz (SPAR Markt)
Marktplatz 10, 4902 Wolfsegg (SPAR Markt)
Saggau 166, 8453 St. Johann i.S. (SPAR Supermarkt)
Altendorferfeld 2, 4152 Sarleinsbach (SPAR Markt)
Nr. 75, 6215 Achenkirch (SPAR Markt)
Nr. 45, 3912 Grafenschlag (SPAR Markt)
Dorfstraße 66, 6580 St. Anton (SPAR Supermarkt)
Grießkirchner Straße 8, 4712 Michaelnbach (SPAR Markt)
Hauptstraße 19, 4741 Wendling (SPAR Markt)
Liechtensteinerstraße 90, 6800 Feldkirch (SPAR Markt)
Nr. 234, 8282 Loipersdorf (SPAR Markt)
Arzberger Straße 33, 8162 Passail (SPAR Supermarkt)
Nr. 8, 8350 Hohenbrugg (SPAR Markt)
Gerlos 145, 6281 Gerlos (SPAR Markt)
Alkovnerstraße 1, 4072 Alkoven-Strassham (SPAR Markt)
Hauptstraße 272, 2051 Zellerndorf (SPAR Markt)
33.A Straße 4, 3331 Kematen an der Ybbs (SPAR Supermarkt)
Panoramastraße 2, 7572 Deutsch-Kaltenbrunn (SPAR Markt)
Haus Nr. 62, 6754 Klösterle (SPAR Markt)
Nr. 12, 8573 Kainach-Voitsberg (SPAR Markt)
Leutenhofen 21, 6914 Hohenweiler (SPAR Markt)
Jagdbergstraße 200, 6822 Schnifis (SPAR Markt)
Sportplatzgasse 4/1, 7412 Wolfau (SPAR Markt)
Hartberger Strasse 1, 7411 Markt Allhau (SPAR Markt)
Nr. 4, 4725 St. Aegidi (SPAR Markt)
Gusenbachstraße 18, 4209 Engerwitzdorf (SPAR Supermarkt)
Fisching 37, 8741 Weißkirchen (SPAR Markt)
Schwimmmbadstraße 14, 8820 Neumarkt (SPAR Markt)
Nr. 181, 8843 St. Peter am Kammersberg (SPAR Markt)
Haupstraße 54, 8763 Möderbrugg (SPAR Markt)
Nr. 84, 8483 Deutsch Goritz (SPAR Markt)
Obere Marktstraße 54, 5541 Altenmarkt i. Pg. (SPAR Supermarkt)
Nr. 109, 8943 Aigen (SPAR Markt)
An der Bundesstraße 446, 8983 Bad Mitterndorf (SPAR Markt)
Nr. 152, 8931 Landl (SPAR Supermarkt)
Ramsau 380, 8972 Ramsau (SPAR Markt)
Nr. 52, 8933 St. Gallen (SPAR Markt)
Nr. 221, 8960 Öblarn (SPAR Markt)
Landstraße 6, 4652 Steinerkirchen (SPAR Supermarkt)
Nr. 170, 3345 Göstling / Ybbs (SPAR Supermarkt)
Haidershofen 3A, 4431 Haidershofen (SPAR Markt)
Wiener Straße 21/2, 3382 Loosdorf (SPAR Supermarkt)
Hofmark 12, 5622 Goldegg (SPAR Markt)
Hauptplatz 4, 3355 Ertl (SPAR Markt)
Kaigasse 28-30, 5020 Salzburg (SPAR Markt)
Altendorf 17, 4793 St. Roman (SPAR Markt)
Nr. 5, 5241 Maria Schmolln (SPAR Markt)
Dorf 10, 6392 St. Jakob im Haus (SPAR Markt)
Unterer Stadtplatz 27-29, 6330 Kufstein (SPAR Markt)
Urichstraße 43, 6500 Landeck (SPAR Markt)
Welthandelsplatz 1, Gebäude D2, Top 3, 1020 Wien (SPAR Markt)
AKH Währinger Gürtel 18-20, 1090 Wien (SPAR Markt)
Dorf 9, 6863 Egg-Großdorf (SPAR Markt)
Lichteneggerstrasse 2/1, 4872 Neukirchen (SPAR Supermarkt)
Altenbergerstraße 69, 4040 Linz (SPAR Markt)
Kirchengasse 43, 2451 Au (SPAR Markt)
Unterlinden 27, 6922 Wolfurt (SPAR Markt)
Schulstraße 1, 4491 Niederneukirchen (SPAR Markt)
Mühlbach 60, 5505 Mühlbach am Hochkönig (SPAR Markt)
Nr. 30, 8863 Stadl-Predlitz (SPAR Markt)
Nr. 522, 6290 Mayrhofen (SPAR Markt)
Nr. 201, 6404 Polling (SPAR Markt)
Haus Nr. 40, 6787 Gargellen (SPAR Markt)
Nr. 73, 8503 St. Josef (SPAR Markt)
Seestraße 1, 7201 Neudörfl (SPAR Supermarkt)
Brucker Bundesstraße 4, 5700 Zell am See (SPAR Markt)
Neudorf 30, 3351 Weistrach (SPAR Markt)
Dorfstraße 11, 6782 Silbertal (SPAR Markt)
Burgenlandstraße 3, 7350 Oberpullendorf (SPAR Supermarkt)
Hauptstraße 97, 6553 See / Paznaun (SPAR Markt)
Kaindorf 336, 8224 Kaindorf b. Hartberg (SPAR Markt)
Kremsmünsterer Str. 5-7, 4030 Linz (SPAR Supermarkt)
Nr. 19, 4252 Liebenau (SPAR Markt)
Balldorf 8, 3304 St. Georgen am Ybbsfelde (SPAR Supermarkt)
Rosseggerstraße 17, 8670 Krieglach (SPAR Supermarkt)
Dorf 45, 6334 Schwoich (SPAR Markt)
Pittermann 92, 8254 Wenigzell (SPAR Markt)
Nr. 409, 8212 Pischelsdorf (SPAR Supermarkt)
Nr. 408, 8212 Pischelsdorf (SPAR Supermarkt)
Laaer Straße 14-16, 2170 Poysdorf (SPAR Supermarkt)
Paracelsusstaße 26, 5020 Salzburg (SPAR Markt)
Hauptplatz 30, 8832 Oberwölz (SPAR Markt)
Kirchplatz 7, 6406 Oberhofen (SPAR Markt)
Hauptplatz 7, 3820 Raabs a.d. Thaya (SPAR Supermarkt)
Lagerhausgasse 38, 2840 Grimmenstein (SPAR Markt)
Nr. 41, 8211 Gr. Pesendorf (SPAR Markt)
Nr. 327, 6262 Schlitters (SPAR Supermarkt)
Dorf 10, 6130 Pill (SPAR Markt)
Urstein Süd 2, 5412 Puch bei Hallein (SPAR Supermarkt)
Nr. 785, 6236 Alpbach (SPAR Markt)
St. Anton Nr. 10, 3283 St. Anton a.d. Jessnitz (SPAR Supermarkt)
Nr. 82, 8091 Jagerberg (SPAR Markt)
Nr. 23, 5242 St. Johann am Walde (SPAR Markt)
Reichstraße 48B, 6890 Lustenau (SPAR Supermarkt)
Landshagerstraße 2, 4113 St. Martin (SPAR Markt)
Nr. 242, 8333 Riegersburg (SPAR Supermarkt)
Nr. 28, 8674 Rettenegg (SPAR Markt)
Nr. 157, 8672 St. Kathrein a. H. (SPAR Markt)
Industriezone 32, 6460 Imst (SPAR Supermarkt)
Radetzkystraße 43, 6020 Innsbruck (SPAR Supermarkt)
Landstraße 1, 3362 Oed-Öhling (SPAR Supermarkt)
Brauhausstraße 92, 2320 Schwechat-Rannersdorf (SPAR Supermarkt)
Nr. 31, 8352 Unterlamm (SPAR Markt)
Arzberg 2, 8253 Waldbach (SPAR Markt)
Preßbaumerstraße 29, 3443 Sieghartskirchen (SPAR Supermarkt)
Nr. 54, 8242 St. Lorenzen a. W. (SPAR Markt)
Hauptstraße 247, 7062 St. Margarethen (SPAR Supermarkt)
Mariazellerstraße 7-9, 3100 St. Pölten (SPAR Supermarkt)
Kirchen, Oberau 481, 6311 Wildschönau (SPAR Supermarkt)
Nr. 322, 6314 Wildschönau (SPAR Supermarkt)
Dorfplatz 7, 6305 Itter (SPAR Markt)
Nr. 310, 6555 Kappl (SPAR Markt)
Kriegener Straße 6, 4761 Enzenkirchen (SPAR Markt)
Waldrasterstraße 2a, 6166 Fulpmes (SPAR Supermarkt)
Nr. 34, 3263 Randegg (SPAR Supermarkt)
Treffner Straße 1, 9500 Villach (SPAR Supermarkt)
10.Oktoberstraße 5a, 9131 Grafenstein (SPAR Supermarkt)
Schärdinger Straße 20, 4971 Aurolzmünster (SPAR Supermarkt)
Ignaz-Glaser-Straße 2, 5111 Bürmoos (SPAR Supermarkt)
Laaer Berg Straße 67-69, 1100 Wien (SPAR Supermarkt)
Hauptstraße 93, 9873 Döbriach (SPAR Supermarkt)
Kirchplatz 1, 6341 Ebbs (SPAR Supermarkt)
F.-W.-Raiffeisen-Straße 1b, 5061 Elsbethen (SPAR Supermarkt)
Marckhgott-Platz 2, 4470 Enns (SPAR Supermarkt)
Zemannstraße 48, 4240 Freistadt (SPAR Supermarkt)
Rosentalerstraße 2, 9586 Fürnitz (SPAR Supermarkt)
Conrad-v.-Hötzendorf-Straße 37a, 8010 Graz (SPAR Supermarkt)
Europaplatz 2, 8020 Graz (SPAR Supermarkt)
Kalvarienbergstraße 76-78, 8020 Graz (SPAR Supermarkt)
Lendplatz 31-33, 8020 Graz (SPAR Supermarkt)
Salzachtalstraße 25, 5400 Hallein (SPAR Supermarkt)
Pachern - Hauptstraße 90, 8075 Hart bei Graz (SPAR Supermarkt)
Riedergasse 2/3, 9620 Hermagor (SPAR Supermarkt)
Langgasse 78/80, 6460 Imst (SPAR Supermarkt)
Fürstenweg 42, 6020 Innsbruck (SPAR Supermarkt)
Innrain 36 b, 6020 Innsbruck (SPAR Supermarkt)
Hauptstraße 217, 9201 Krumpendorf (SPAR Supermarkt)
Nr. 554, 9640 Kötschach (SPAR Supermarkt)
Bahnhofplatz 4, 8700 Leoben (SPAR Supermarkt)
Brixnerplatz 1, 9900 Lienz (SPAR Supermarkt)
Defreggenstraße 10, 9900 Lienz (SPAR Supermarkt)
Joh.-Wilhelm-Klein-Straße 13, 4040 Linz (SPAR Supermarkt)
Hanuschstraße 32, 4020 Linz (SPAR Supermarkt)
Stieglbauernstraße 4, 4020 Linz (SPAR Supermarkt)
Ziegeleistraße 87, 4020 Linz (SPAR Supermarkt)
Griesmayrstraße 21, 4040 Linz-Urfahr (SPAR Supermarkt)
Nr. 358, 5090 Lofer (SPAR Supermarkt)
Johann-Wöckinger-Straße 1, 4209 Mittertreffling (SPAR Supermarkt)
St. Egidi 76a, 8850 St. Egidi (SPAR Supermarkt)
Statz 90, 6143 Mühlbachl b. Matrei (SPAR Supermarkt)
Wienerstraße 24, 8820 Neumarkt (SPAR Supermarkt)
Nr. 332, 9611 Nötsch (SPAR Supermarkt)
Ragnitz 6, 8413 Ragnitz (SPAR Supermarkt)
Vogelmaierweg 5, 5661 Rauris (SPAR Supermarkt)
Eberhard-Fugger-Straße 5, 5020 Salzburg (SPAR Supermarkt)
Berchtesgadenerstraße 35a, 5020 Salzburg (SPAR Supermarkt)
Südtirolerpl. 1, 5020 Salzburg (SPAR Supermarkt)
Itzlinger Hauptstraße 93 a, 5020 Salzburg (SPAR Supermarkt)
Kendlerstraße 26, 5020 Salzburg (SPAR Supermarkt)
Klostermaierhofweg 1, 5020 Salzburg (SPAR Supermarkt)
Marktplatz 1, 5620 Schwarzach i.Pg. (SPAR Supermarkt)
Hauptstraße 26, 9871 Seeboden (SPAR Supermarkt)
Drauweg 25, 9800 Spittal (SPAR Supermarkt)
Attergaustraße 60, 4880 St. Georgen (SPAR Supermarkt)
Speckbacherstraße 10, 6380 St. Johann i.T. (SPAR Supermarkt)
Markt 138, 8323 St. Marein bei Graz (SPAR Supermarkt)
Hauptstraße 1, 8641 St. Marein (SPAR Supermarkt)
Stelzhammerstraße 15, 4651 Stadl Paura (SPAR Supermarkt)
Hans Wagner Straße 2 - 4, 4400 Steyr (SPAR Supermarkt)
Leop. Werndl-Straße 25, 4400 Steyr (SPAR Supermarkt)
Am Kreisverkehr 1, 8772 Traboch (SPAR Supermarkt)
Leondingerstraße 50, 4050 Traun - St. Martin (SPAR Supermarkt)
Anton Hesch-Gasse 2, 4840 Vöcklabruck (SPAR Supermarkt)
Aflingerstraße 11, 6176 Völs (SPAR Supermarkt)
Freisinger Berg 6, 3340 Waidhofen an der Ybbs (SPAR Supermarkt)
Werdenbergstraße 34, 6700 Bludenz (SPAR Supermarkt)
Leutbühel 2, 6900 Bregenz (SPAR Supermarkt)
Dr. A. Schneider Straße 15b, 6850 Dornbirn (SPAR Supermarkt)
Leopoldstraße 1, 6850 Dornbirn (SPAR Supermarkt)
Rohrbachstraße 59, 6850 Dornbirn (SPAR Supermarkt)
Bundesstraße 80, 6972 Fussach (SPAR Supermarkt)
Ybbs Strasse 11-13, 3300 Amstetten (SPAR Supermarkt)
Füstenfelder Straße 10, 8200 Gleisdorf (SPAR Supermarkt)
Conrad von Hötzendorfstraße 99a, 8010 Graz (SPAR Supermarkt)
Grüne Gasse 19b, 8020 Graz (SPAR Supermarkt)
Moserhofgasse 44, 8010 Graz (SPAR Supermarkt)
Radetzkystraße 1-3, 8010 Graz (SPAR Supermarkt)
Straßganger Straße 229, 8052 Graz (SPAR Supermarkt)
Waagner-Biro-Straße 84, 8020 Graz (SPAR Supermarkt)
Waltendorfer Hauptstraße 31b/EG, 8010 Graz (SPAR Supermarkt)
Kirchstraße 28, 6091 Götzens (SPAR Supermarkt)
Lindet 2, 4753 Taiskirchen (SPAR Supermarkt)
Davidgasse 79 - 81, 1100 Wien (SPAR Supermarkt)
Neue Mitte 3, 5122 Hochburg-Ach (SPAR Supermarkt)
Hauptstrasse 79, 2214 Auersthal (SPAR Supermarkt)
Nr. 221, 8082 Kirchbach (SPAR Supermarkt)
Grazerstraße 48g, 8062 Kumberg (SPAR Supermarkt)
Traunstraße 61, 5026 Salzburg (SPAR Supermarkt)
Aglassingerstraße 40, 5023 Salzburg-Gnigl (SPAR Supermarkt)
Nauders 521, 6543 Nauders (SPAR Supermarkt)
Wr. Neust. Str. 31, 2821 Lanzenkirchen (SPAR Supermarkt)
Gramatneusiedlier Straße 29, 2435 Ebergassing (SPAR Supermarkt)
Zur Hängebrücke 1, 6422 Stams (SPAR Supermarkt)
Wienerstraße 6, 7551 Stegersbach (SPAR Supermarkt)
Dorf Nr. 46a, 6652 Elbigenalp (SPAR Supermarkt)
Dorfstraße 4, 6712 Thüringen (SPAR Supermarkt)
Pyrawanger Straße 2, 4092 Esternberg (SPAR Supermarkt)
Niederschöckelstraße 54a, 8044 Weinitzen (SPAR Supermarkt)
Linzer Straße 1, 4701 Bad Schallerbach (SPAR Supermarkt)
Bahnhofstraße 42, 6300 Wörgl (SPAR Supermarkt)
Hauptstraße 16, 7301 Deutschkreutz (SPAR Supermarkt)
Unt. Hauptstraße 8, 7223 Sieggraben (SPAR Markt)
Katzenberg 19, 4982 Kirchdorf am Inn (SPAR Markt)
Linzergasse 57, 5020 Salzburg (SPAR Markt)
Linzergasse 17-19, 5020 Salzburg (SPAR Markt)
Gerlos 230, 6281 Gerlos (SPAR Markt)
Nr. 94, 8611 St. Katharein a.d. L. (SPAR Markt)
Hauptplatz 21, 8130 Frohnleiten (SPAR Markt)
Langenlebarner Straße 55-57, 3430 Tulln (SPAR Supermarkt)
Nr. 54, 8484 Unterpurkla (SPAR Markt)
Nr. 65, 5742 Königsleiten (SPAR Markt)
Hauptstraße 9, 8920 Hieflau (SPAR Markt)
Brunnenstraße 20 a, 3830 Waidhofen (SPAR Supermarkt)
Haupstrasse 28, 8786 Rottenmann (SPAR Supermarkt)
Mariazeller Straße 5a, 3180 Lilienfeld (SPAR Supermarkt)
Hainfelder Bundesstraße 4, 3160 Traisen (SPAR Supermarkt)
Nr. 54, 6767 Warth (SPAR Markt)
Nr. 19, 8861 St. Georgen / Murau (SPAR Markt)
Nr. 351, 2061 Hadres (SPAR Supermarkt)
Nr. 360, 3610 Weißenkirchen (SPAR Supermarkt)
Nr. 138, 8322 Studenzen (SPAR Supermarkt)
Nr. 78, 8362 Söchau (SPAR Markt)
Untere Hauptstraße 19, 8663 St. Barbara i.M. (SPAR Supermarkt)
Postplatz 6, 5582 St. Michael (SPAR Markt)
Johann-Löckerstraße 2, 5580 Tamsweg (SPAR Supermarkt)
Bierbaum 87, 8093 St. Peter/O. (SPAR Markt)
Nr. 42, 8262 Ilz (SPAR Supermarkt)
Hauptstraße 8, 5165 Berndorf b. Salzburg (SPAR Markt)
Zehnergasse 1, 2700 Wr. Neustadt (SPAR Supermarkt)
Fischauergasse 58b, 2700 Wr. Neustadt (SPAR Supermarkt)
Kaisersteingasse 113, 2700 Wr. Neustadt (SPAR Supermarkt)
Oberwarterstraße 8, 7461 Stadt Schlaining (SPAR Markt)
Nöstelbachstraße 22, 4502 St. Marien (SPAR Markt)
Ob. Hauptstraße 53-55, 7142 Illmitz (SPAR Supermarkt)
Nr. 291, 8151 Hitzendorf (SPAR Supermarkt)
Wr. Neustädter Straße 56, 2601 Sollenau (SPAR Express)
Vöslauer Straße 77, 2500 Baden (SPAR Express)
Grenzackerstraße 2-4, 1100 Wien (SPAR Express)
Bahnhofstraße 46, 4810 Gmunden (SPAR Express)
Pichlhof 19, 8970 Haus (SPAR Express)
Salzburgerstraße 90, 5500 Bischofshofen (SPAR Express)
Zellerstraße 30, 5730 Mittersills (SPAR Express)
Braunauerstraße 10, 4910 Ried im Innkreis (SPAR Express)
Dinghoferstraße 35-37, 4020 Linz (SPAR Express)
Unionstraße 55, 4020 Linz (SPAR Express)
Pragerstraße 3, 4240 Freistadt (SPAR Express)
Goethestraße 101, 4020 Linz (SPAR Express)
Muldenstraße 2, 4020 Linz (SPAR Express)
Weitenfeld 5, 4048 Puchenau (SPAR Express)
Pachergasse 14, 4400 Steyr (SPAR Express)
Bockgasse 30, 4020 Linz (SPAR Express)
Horner Straße 25, 3902 Vitis (SPAR Express)
P.B.Rodlberger Straße 39, 4600 Thalheim bei Wels (SPAR Express)
Linzer Straße 189, 4600 Wels (SPAR Express)
Krottenbachstraße 233, 1190 Wien (SPAR Express)
Ungargasse 22, 2700 Wiener Neustadt (SPAR Express)
Schwadorfer Straße, 2435 Ebergassing (SPAR Express)
Wiener Straße 95, 2020 Hollabrunn (SPAR Express)
Laaer-Berg-Straße 207, 1100 Wien (SPAR Express)
Ressavarstraße 50, 8230 Hartberg (SPAR Express)
Landstrasse 139, 2410 Hainburg (SPAR Express)
Wiener Strasse 437, 4020 Linz (SPAR Express)
Franz Brötznerstr. 2, 5071 Wals (SPAR Express)
Innsbrucker Bundesstraße 97, 5020 Salzburg (SPAR Express)
Eggersdorfer Str.6, 3330 Amstetten (SPAR Express)
Hauptstraße 2, 4642 Sattledt (SPAR Express)
Klaus/Pyhrnbahn 147, 4564 Klaus/Phyrnbahn (SPAR Express)
Brunnerstrasse B12A, 2345 Brunn am Gebirge (SPAR Express)
Altmannsd. Straße 150, 1230 Wien (SPAR Express)
Hauptplatz 9-11, 3435 Zwentendorf (SPAR Supermarkt)
Franz-Senn-Straße 93, 6167 Neustift im Stubaital (SPAR Markt)
Walserstraße 74/1, 6991 Riezlern (SPAR Markt)
Dorf 300, 6942 Krumbach (SPAR Markt)
Am Rathausplatz 6, 6900 Bregenz (SPAR Supermarkt)
Haselstauderstraße 16, 6850 Dornbirn (SPAR Supermarkt)
Schulstraße 7, 9562 Himmelberg (SPAR Markt)
Wiener Strasse 114, 2700 Wr. Neustadt (SPAR Express)
Leombach 15, 4621 Sipbachzell (SPAR Express)
Steyrer Straße 93, 3353 Seitenstetten (SPAR Express)
Salzburger Straße 146, 4600 Wels (SPAR Express)
Pragerstraße 155, 1210 Wien (SPAR Express)
Weißbriach 233, 9622 Weißbriach (SPAR Supermarkt)
Steuerberg 50, 9560 Steuerberg/Feldkirchen (SPAR Markt)
Geschäftsstraße 2, 9342 Gurk (SPAR Supermarkt)
Eferdinger Straße 81, 4600 Wels (SPAR Express)
Ferdinand-Raimund-Straße 1, 3300 Amstetten (SPAR Supermarkt)
Ebenau Reichenau 89, 9565 Ebene Reichenau (SPAR Markt)
Gallizien 6, 9132 Gallizien (SPAR Markt)
Unterer Markt 6, 9334 Guttaring (SPAR Markt)
Feldkirchner Straße 380, 9061 Wölfnitz (SPAR Supermarkt)
Globasnitz 30, 9142 Globasnitz (SPAR Markt)
Bad Eisenkappel 333, 9135 Bad Eisenkappel (SPAR Markt)


[1]: https://spar-international.com/country/austria/
