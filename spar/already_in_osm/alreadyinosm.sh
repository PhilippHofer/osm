#!/bin/bash
while read p; do
	addr=$(echo "$p" | cut -d"(" -f1)
	details=$(curl -s --get "https://www.openstreetmap.org/geocoder/search_osm_nominatim" --data-urlencode "query=$addr")
	if [[ "$details" =~ "par" ]]; then
		echo "$p;yes"
	else
		echo "$p;no"
	fi
done<../details_spar/spar_addresses.txt

