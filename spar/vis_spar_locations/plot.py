import pandas as pd
import matplotlib.pyplot as plt
from shapely.geometry import Point
import geopandas as gpd
from geopandas import GeoDataFrame

df = pd.read_csv("../spar_locations.csv", delimiter=',', skiprows=0, low_memory=False)

geometry = [Point(xy) for xy in zip(df['Latitude'], df['Longitude'])]
gdf = GeoDataFrame(df, geometry=geometry)   

#this is a simple map that goes with geopandas
world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

world_filepath = gpd.datasets.get_path('naturalearth_lowres')
world = gpd.read_file(world_filepath)
world.head()
singapore = world.loc[world['name'] == 'Austria'] # get Singapore row
boundaries = singapore['geometry'] # get Singapore geometry


gdf.plot(ax=singapore.plot(figsize=(20, 14)), marker='o', color='red', markersize=15);
plt.savefig('spar.png')
